import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.*;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class TestAvic {
    private WebDriver driver;

    @BeforeTest
    public void preSetup() {
        System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver.exe");
    }

    @BeforeMethod
    public void testSetup() {
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("https://avic.ua/");
    }

    @Test(priority = 1)
    public void checkRightSearch() {
        driver.findElement(By.xpath("//input[@class='search-query']")).sendKeys("Xiaomi Mi 11");
        driver.findElement(By.xpath("//button[contains(@class, 'search-btn') and @type='submit']")).click();
        assertTrue(driver.getCurrentUrl().contains("query=Xiaomi+Mi+11"));
    }

    @Test(priority = 2)
    public void checkAddItem() {
        String[] list = new String[]{"Apple Store", "Смартфоны и телефоны", "Компьютеры", "Ноутбуки и планшеты", "Телевизоры и аксессуары",
                "Бытовая техника", "Аудиотехника", "Гаджеты", "Аксессуары", "Game Zone", "Фото-видео техника", "Умный дом", "Электротранспорт"};
        int i = 0;

        driver.get("https://avic.ua/blog");
        assertTrue(driver.getCurrentUrl().contains("https://avic.ua/blog"));
        driver.findElement(By.xpath("//span[@class='sidebar-item']")).click();
        List<WebElement> elements = driver.findElements(By.xpath("//ul[@class='sidebar-list sidebar-list--fl']/li[@class='parent js_sidebar-item']//span[@class='sidebar-item-title']/span"));
        for (WebElement element : elements) {
            assertEquals(element.getText(), list[i]);
            i++;
        }
    }

    @Test(priority = 3)
    public void checkChangeLanguage(){
        String url = driver.findElement(By.xpath("//li[@class='has-dropdown']/ul[contains(@class,'menu-dropdown__lang')]//a")).getAttribute("href");
        driver.get(url);
        assertEquals(driver.findElement(By.xpath("//ul[contains(@class,'two-column')]//a[contains(@href,'dostavka-tovarov')]")).getText(),
                "Оплата і доставка");
        assertEquals(driver.findElement(By.xpath("//ul[contains(@class,'two-column')]//a[contains(@href,'optovyij-otdel')]")).getText(),
                "Корпоративний відділ");
    }

    @Test(priority = 4)
    public void checkRightDiscount(){
        driver.findElement(By.xpath("//input[@class='search-query']")).sendKeys("Xiaomi");
        driver.findElement(By.xpath("//button[contains(@class, 'search-btn') and @type='submit']")).click();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        String discount = driver.findElement(By.xpath("//div[@class='item-prod col-lg-3']//div[contains(@class,'bg-orange')]")).getText();
        String price = driver.findElement(By.xpath("//div[@class='item-prod col-lg-3']//div[contains(@class,'bg-orange')]/parent::div/parent::div//div[@class='prod-cart__prise-new']")).getText();
        String oldPrice = driver.findElement(By.xpath("//div[@class='item-prod col-lg-3']//div[@class='prod-cart__prise-old']")).getText();

        int intOldPrice = Integer.parseInt(oldPrice.substring(0,oldPrice.length()-4));
        int intPrice = Integer.parseInt(price.substring(0,price.length()-4));
        int intDiscount = Integer.parseInt(discount.substring(9,discount.length()-4));
        //System.out.println(intOldPrice +" - "+ intPrice +" = "+ intDiscount);
        assertEquals(intDiscount, intOldPrice - intPrice);


    }

    @AfterMethod
    public void postSetup() {
        driver.quit();
    }
}
